﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bowling.Services.Models
{
    public class Joueur
    {
        private string _nom { get; set; }

        private List<JoueurTour> _tours { get; set; }

        public Joueur(string nom)
        {
            _nom = nom;
            _tours = new List<JoueurTour>();
        }

        public string getNom() { return _nom; }

        public int getPoints()
        {
            int result = 0;

            foreach(var tour in _tours)
            {
                result += tour.getPoints();
            }

            return result;
        }

        public List<JoueurTour> getTours() { return _tours; }

        public int joue()
        {
            var tour = new JoueurTour(_tours.Count != 0 ? _tours[_tours.Count -1] : null );
            tour.jouerTour();
            _tours.Add(tour);
            return tour.getPoints();
        }

        public override string ToString()
        {
            return _nom + " (" + getPoints() + ")";
        }
    }
}
