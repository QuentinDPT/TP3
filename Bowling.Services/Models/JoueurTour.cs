﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bowling.Services.Models
{
    public class JoueurTour
    {
        private int _tour { get; set; }

        private List<Quille> _quilles { get; set; }

        private JoueurTour _dernierTour { get; set; }

        public JoueurTour(JoueurTour dernierTour = null)
        {
            _dernierTour = dernierTour;
            _quilles = new List<Quille>();
            for (int nbQuilles = 0; nbQuilles < 10; nbQuilles++)
                _quilles.Add(new Quille());
            _tour = 0;
        }

        public int jouerTour()
        {
            _jouerTour();
            if (_quilles.Where(x => x.getTourTombe() == 0).Count() == 0)
                return 1;
            _jouerTour(2);
            return 2;
        }

        private void _jouerTour(int tour = 1)
        {
            var aTomber = _quilles.Where(x => x.getTourTombe() == 0).ToList();
            
            foreach(var q in aTomber)
            {
                if (new Random().Next(100) > 50)
                    q.quilleTombe(tour);
            }

            _tour++; 
        }

        public int getPoints()
        {
            int result = 0;

            int multiplier1 = (_dernierTour != null && _dernierTour.getQuilles().Where(x => x.getTourTombe() >= 1).Count() == 10) ? 2 : 1;
            int multiplier2 = (_dernierTour != null && _dernierTour.getQuilles().Where(x => x.getTourTombe() == 1).Count() == 10) ? 2 : 1;

            result = _quilles.Where(x => x.getTourTombe() == 1).Count()*multiplier1 + _quilles.Where(x => x.getTourTombe() == 2).Count()*multiplier2;

            return result;
        }

        public List<Quille> getQuilles()
        {
            return _quilles;
        }

        public override string ToString()
        {
            string lancer1 = _quilles.Where(x => x.getTourTombe() == 1).Count().ToString();
            string lancer2 = _quilles.Where(x => x.getTourTombe() == 2).Count().ToString();

            if(lancer1 == "10")
            {
                lancer1 = "X";
                lancer2 = "-";
            }
            if(int.Parse(lancer2)+int.Parse(lancer1) == 10)
            {
                lancer2 = "S";
            }
            
            return lancer1 + " - " + lancer2;
        }
    }
}
