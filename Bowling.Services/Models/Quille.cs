﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bowling.Services.Models
{
    public class Quille
    {
        private int _quilleId { get; set; }

        private int _tourTomber { get; set; }

        public Quille()
        {
            _tourTomber = 0;
        }

        public int getTourTombe()
        {
            return _tourTomber;
        }

        public void quilleTombe(int lancer)
        {
            _tourTomber = lancer;
        }
    }
}
