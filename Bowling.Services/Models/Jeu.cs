﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bowling.Services.Models
{
    public class Jeu
    {
        private int _nbTours { get; set; }

        private int _tour { get; set; }

        private Joueur _joueurActuel { get; set; }

        private int _joueurActuelIdx { get; set; }

        private List<Joueur> _joueurs { get; set; }

        public Jeu(int nbJoueurs)
        {
            if (nbJoueurs <= 1)
                throw new ArgumentException("Vous ne pouvez pas jouer seul");

            _nbTours = 10;
            _tour = 1;
            _joueurs = new List<Joueur>();
            for (int i = 0; i < nbJoueurs; i++)
                _joueurs.Add(new Joueur(i+1+". Roberto"));
            _joueurActuel = _joueurs[0];
        }

        public int getNbJoueurs() { return _joueurs.Count ; }

        public int getTour() { return _tour; }

        public string getJoueurActuel() { return _joueurActuel.getNom(); }

        public List<Joueur> getJoueurs()
        {
            return _joueurs;
        }

        public void joueurSuivant()
        {
            if (estFini())
                return;

            _joueurActuel.joue();

            if (_joueurActuelIdx == _joueurs.Count - 1)
            {
                _tour++;
                _joueurActuel = _joueurs[0];
                _joueurActuelIdx = 0;
            }
            else
            {
                _joueurActuel = _joueurs[++_joueurActuelIdx];
            }
        }

        public void tourSuivant()
        {
            var t = _tour;
            while (_joueurActuelIdx != 0 || t == _tour)
                joueurSuivant();
        }

        public bool estFini()
        {
            return _tour >= _nbTours;
        }
    }
}
