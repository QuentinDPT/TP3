﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FizzBuzz.Services.Models
{
    public class Jeu
    {
        private int _nombre;

        public Jeu(int nombre)
        {
            if (nombre < 15 || nombre > 150)
                throw new ArgumentException("le nombre choisis ne fait pas parti de la plage de valeurs");

            _nombre = nombre;
        }

        public List<string> getList()
        {
            var result = new List<string>();

            for(int i = 1; i<=_nombre; i++)
            {
                string nombre = "";
                
                if (i % 3 == 0)
                    nombre = "Fizz";
                if (i % 5 == 0)
                    nombre += "Buzz";

                if (nombre == "")
                    nombre = i.ToString();

                result.Add(nombre);
            }

            return result;
        }


    }
}
