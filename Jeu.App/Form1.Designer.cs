﻿
namespace Jeu.App
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.JeuTabControl = new System.Windows.Forms.TabControl();
            this.BowlingTabPage = new System.Windows.Forms.TabPage();
            this.BowlingList = new System.Windows.Forms.ListBox();
            this.BowlingNextTurn = new System.Windows.Forms.Button();
            this.BowlingReload = new System.Windows.Forms.Button();
            this.FizzBuzzTabPage = new System.Windows.Forms.TabPage();
            this.FBNumeric = new System.Windows.Forms.NumericUpDown();
            this.FBButton = new System.Windows.Forms.Button();
            this.FBList = new System.Windows.Forms.ListBox();
            this.JeuTabControl.SuspendLayout();
            this.BowlingTabPage.SuspendLayout();
            this.FizzBuzzTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FBNumeric)).BeginInit();
            this.SuspendLayout();
            // 
            // JeuTabControl
            // 
            this.JeuTabControl.Controls.Add(this.BowlingTabPage);
            this.JeuTabControl.Controls.Add(this.FizzBuzzTabPage);
            this.JeuTabControl.Location = new System.Drawing.Point(12, 12);
            this.JeuTabControl.Name = "JeuTabControl";
            this.JeuTabControl.SelectedIndex = 0;
            this.JeuTabControl.Size = new System.Drawing.Size(1058, 482);
            this.JeuTabControl.TabIndex = 0;
            // 
            // BowlingTabPage
            // 
            this.BowlingTabPage.Controls.Add(this.BowlingList);
            this.BowlingTabPage.Controls.Add(this.BowlingNextTurn);
            this.BowlingTabPage.Controls.Add(this.BowlingReload);
            this.BowlingTabPage.Location = new System.Drawing.Point(4, 24);
            this.BowlingTabPage.Name = "BowlingTabPage";
            this.BowlingTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.BowlingTabPage.Size = new System.Drawing.Size(1050, 454);
            this.BowlingTabPage.TabIndex = 0;
            this.BowlingTabPage.Text = "Bowling";
            this.BowlingTabPage.UseVisualStyleBackColor = true;
            // 
            // BowlingList
            // 
            this.BowlingList.FormattingEnabled = true;
            this.BowlingList.ItemHeight = 15;
            this.BowlingList.Location = new System.Drawing.Point(3, 3);
            this.BowlingList.Name = "BowlingList";
            this.BowlingList.Size = new System.Drawing.Size(916, 439);
            this.BowlingList.TabIndex = 2;
            // 
            // BowlingNextTurn
            // 
            this.BowlingNextTurn.Location = new System.Drawing.Point(925, 399);
            this.BowlingNextTurn.Name = "BowlingNextTurn";
            this.BowlingNextTurn.Size = new System.Drawing.Size(122, 23);
            this.BowlingNextTurn.TabIndex = 1;
            this.BowlingNextTurn.Text = "Tour suivant";
            this.BowlingNextTurn.UseVisualStyleBackColor = true;
            this.BowlingNextTurn.Click += new System.EventHandler(this.BowlingNextTurn_Click);
            // 
            // BowlingReload
            // 
            this.BowlingReload.Location = new System.Drawing.Point(925, 428);
            this.BowlingReload.Name = "BowlingReload";
            this.BowlingReload.Size = new System.Drawing.Size(122, 23);
            this.BowlingReload.TabIndex = 0;
            this.BowlingReload.Text = "Reload";
            this.BowlingReload.UseVisualStyleBackColor = true;
            this.BowlingReload.Click += new System.EventHandler(this.BowlingReload_Click);
            // 
            // FizzBuzzTabPage
            // 
            this.FizzBuzzTabPage.Controls.Add(this.FBNumeric);
            this.FizzBuzzTabPage.Controls.Add(this.FBButton);
            this.FizzBuzzTabPage.Controls.Add(this.FBList);
            this.FizzBuzzTabPage.Location = new System.Drawing.Point(4, 24);
            this.FizzBuzzTabPage.Name = "FizzBuzzTabPage";
            this.FizzBuzzTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.FizzBuzzTabPage.Size = new System.Drawing.Size(1050, 454);
            this.FizzBuzzTabPage.TabIndex = 1;
            this.FizzBuzzTabPage.Text = "FizzBuzz";
            this.FizzBuzzTabPage.UseVisualStyleBackColor = true;
            // 
            // FBNumeric
            // 
            this.FBNumeric.Location = new System.Drawing.Point(914, 419);
            this.FBNumeric.Name = "FBNumeric";
            this.FBNumeric.Size = new System.Drawing.Size(52, 23);
            this.FBNumeric.TabIndex = 2;
            this.FBNumeric.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.FBNumeric.ValueChanged += new System.EventHandler(this.FBNumeric_ValueChanged);
            // 
            // FBButton
            // 
            this.FBButton.Location = new System.Drawing.Point(972, 419);
            this.FBButton.Name = "FBButton";
            this.FBButton.Size = new System.Drawing.Size(75, 23);
            this.FBButton.TabIndex = 1;
            this.FBButton.Text = "Choisir";
            this.FBButton.UseVisualStyleBackColor = true;
            this.FBButton.Click += new System.EventHandler(this.FBButton_Click);
            // 
            // FBList
            // 
            this.FBList.FormattingEnabled = true;
            this.FBList.ItemHeight = 15;
            this.FBList.Location = new System.Drawing.Point(3, 3);
            this.FBList.Name = "FBList";
            this.FBList.Size = new System.Drawing.Size(905, 439);
            this.FBList.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1082, 506);
            this.Controls.Add(this.JeuTabControl);
            this.Name = "Form1";
            this.Text = "Jeu";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.JeuTabControl.ResumeLayout(false);
            this.BowlingTabPage.ResumeLayout(false);
            this.FizzBuzzTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FBNumeric)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl JeuTabControl;
        private System.Windows.Forms.TabPage BowlingTabPage;
        private System.Windows.Forms.TabPage FizzBuzzTabPage;
        private System.Windows.Forms.Button BowlingReload;
        private System.Windows.Forms.Button BowlingNextTurn;
        private System.Windows.Forms.ListBox BowlingList;
        private System.Windows.Forms.Button FBButton;
        private System.Windows.Forms.ListBox FBList;
        private System.Windows.Forms.NumericUpDown FBNumeric;
    }
}

