﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jeu.App
{
    public partial class Form1 : Form
    {
        private Bowling.Services.Models.Jeu bowlingGame;

        private FizzBuzz.Services.Models.Jeu fizzBuzzGame;

        public Form1()
        {
            InitializeComponent();

            bowlingGame = new Bowling.Services.Models.Jeu(2);
            fizzBuzzGame = new FizzBuzz.Services.Models.Jeu(15);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DataRefresh();
        }

        private void BowlingReload_Click(object sender, EventArgs e)
        {
            bowlingGame = new Bowling.Services.Models.Jeu(2);
            DataRefresh();
        }

        private void BowlingNextTurn_Click(object sender, EventArgs e)
        {
            bowlingGame.joueurSuivant();
            DataRefresh();
        }

        private void DataRefresh()
        {
            var joueurs = bowlingGame.getJoueurs();

            BowlingList.Items.Clear();
            foreach (var joueur in joueurs)
            {
                foreach(var joueurTour in joueur.getTours())
                {
                    BowlingList.Items.Add(joueur + " : " + joueurTour);
                }
            }
        }

        private void FBButton_Click(object sender, EventArgs e)
        {
            FBList.Items.Clear();

            foreach (var element in fizzBuzzGame.getList())
            {
                FBList.Items.Add(element);
            }
        }

        private void FBNumeric_ValueChanged(object sender, EventArgs e)
        {
            int value = int.Parse(FBNumeric.Value.ToString());

            if (value < 15 || value > 150)
                return;

            fizzBuzzGame = new FizzBuzz.Services.Models.Jeu(value);
        }
    }
}
