using System;
using Xunit;

namespace Bowling.Test
{
    public class JeuComplet
    {
        [Fact]
        public void Senario1()
        {
            var jeu = new Bowling.Services.Models.Jeu(3);

            jeu.getJoueurActuel();

            jeu.getNbJoueurs();

            jeu.getTour();

            while (!jeu.estFini())
                jeu.tourSuivant();

        }
    }
}
